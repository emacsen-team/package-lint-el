package-lint-el (0.24-3) UNRELEASED; urgency=medium

  * Correct the reason for skipping tests due to requiring melpa and
    networking
    - This rectifies commits 585bc6c and 70eac1c.
  * Add d/gbp.conf matching the current practice

 -- Xiyue Deng <manphiz@gmail.com>  Fri, 07 Mar 2025 04:20:45 -0800

package-lint-el (0.24-2) unstable; urgency=medium

  * Use ert_selectors to skip failing tests
    - Require dh-elpa >= 2.1.6
    - Drop 0002-disable-tests.diff
  * Skip more tests failing due to not allowing separately packaged seq
  * Update Standards-Version to 4.7.2; no change needed
  * Add myself to Uploaders

 -- Xiyue Deng <manphiz@gmail.com>  Thu, 06 Mar 2025 12:00:31 -0800

package-lint-el (0.24-1) unstable; urgency=medium

  * New upstream version 0.24
  * d/control: Declare Standards-Version 4.7.0 (no changes needed)
  * d/rules: Disable upstream build

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 10 Nov 2024 13:03:29 +0500

package-lint-el (0.23-1) unstable; urgency=medium

  * New upstream version 0.23
  * Re-enable fixed tests
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 03 Apr 2024 17:14:25 +0500

package-lint-el (0.22-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Team upload
  * New upstream version 0.22
  * Update copyright year in d/copyright
  * Add Upstream-Contact in d/copyright
  * Modernize d/watch with special substitute string to be more robust

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 29 Mar 2024 09:04:25 +0800

package-lint-el (0.21-1) unstable; urgency=medium

  * New upstream version 0.21
  * Update patches
  * d/control: Build-Depend on elpa-compat
  * d/control: Clean Recommends

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 09 Dec 2023 14:19:38 +0500

package-lint-el (0.19-1) unstable; urgency=medium

  * New upstream version 0.19
  * Drop obsolete patch, refresh patches
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 10 Aug 2023 19:40:51 +0500

package-lint-el (0.17-1) unstable; urgency=medium

  * New upstream version 0.17
  * Refresh patches
  * tmp: remove test patch
  * d/control: Declare Standards-Version 4.6.2 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 18 Jun 2023 10:18:04 +0500

package-lint-el (0.16-1) unstable; urgency=medium

  * New upstream version 0.16
  * Refresh patches
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 26 Feb 2022 10:24:49 +0500

package-lint-el (0.15-1) unstable; urgency=medium

  * New upstream version 0.15
  * Update patches
  * d/control: Declare Standards-Version 4.6.0 (no changes needed)
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 19 Aug 2021 09:38:54 +0500

package-lint-el (0.13-1) unstable; urgency=medium

  * New upstream version 0.13
  * Refresh patches
  * Add some upstream metadata
  * d/control: Migrate to debhelper-compat 13
  * d/copyright: Fix wildcard to correctly match directory
  * d/copyright: Update copyright information

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 13 Aug 2020 09:10:16 +0500

package-lint-el (0.12-1) unstable; urgency=medium

  * New upstream version 0.12
  * Refresh patches
  * d/control: Declare Standards-Version 4.5.0 (no changes needed)
  * d/copyright: Update copyright information

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 27 Feb 2020 22:02:36 +0500

package-lint-el (0.11-1) unstable; urgency=medium

  * New upstream version 0.11
  * Migrate to debhelper 12 without d/compat
  * d/control: Declare Standards-Version 4.4.1 (no changes needed)
  * d/control: Drop emacs25 from Enhances
  * d/control: Clean dependencies
  * d/control: Add elpa-let-alist to Build-Depends
  * Refresh patch to fix version number
  * Refresh patch to clean documentation
  * Refresh patch to deal with tests

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 15 Dec 2019 15:05:30 +0500

package-lint-el (0.7-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 12:17:58 -0300

package-lint-el (0.7-1) unstable; urgency=medium

  * New upstream version 0.7
  * Refresh patches
  * d/control: Declare Standards-Version 4.3.0 (no changes needed)
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 03 Jan 2019 18:36:39 +0500

package-lint-el (0.6-1) unstable; urgency=medium

  * New upstream version 0.6
  * Refresh patches
  * Migrate to dh 11
  * Package new package-lint-flymake as a separate binary package
  * d/control: Add Rules-Requires-Root: no
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org
  * d/control: Declare Standards-Version 4.2.1 (no changes needed)
  * d/control: Update Maintainer (to Debian Emacsen team)

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 20 Nov 2018 17:21:08 +0500

package-lint-el (0.5-1) unstable; urgency=medium

  * Initial release (Closes: #877431)

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 1 Oct 2017 22:12:41 +0500
